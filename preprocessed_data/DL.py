#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.keras.layers import LSTM ,Dense,Embedding,Flatten,Dropout,TimeDistributed,Activation,MaxPooling1D,MaxPooling2D,BatchNormalization
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.constraints import MinMaxNorm
from tensorflow.keras.initializers import RandomNormal

import logging

is_binary = True
test_data = False
learning_rate = 1e-5

opt = tf.keras.optimizers.Adam(lr=learning_rate)
#opt = tf.keras.optimizers.SGD(lr=learning_rate, momentum=0.1)
#opt = tf.keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
logging.getLogger('tensorflow').disabled = True

if test_data:
    input_size = 4000
    class_num = 10
    train_X = np.ones((1000, 4000))
    train_Y = np.ones((1000, 1))
    counter = -1
    for row in range(100):
        for label in range(10):
            counter += 1
            for j in range(4000):
                train_X[counter, j] = label

            train_Y[counter] = label

    #print(train_X)
    #print(train_Y)
else:
    input_size = 4000
    train_X = pd.read_csv('/home/togzhan/Desktop/git/preprocessed_data/full_dataset/ed_data_x.csv')
    train_Y = pd.read_csv('/home/togzhan/Desktop/git/preprocessed_data/full_dataset/labels.csv')
    train_X = train_X.to_numpy()
    train_Y = train_Y.to_numpy()
    fft_size = 64
    input_size = fft_size
    train_X = np.fft.fft(train_X, fft_size, 1)
    train_X = np.abs(train_X)

train_X = np.reshape(train_X, (train_X.shape[0], train_X.shape[1], 1))

indices = np.random.permutation(train_X.shape[0])
train_X = train_X[indices[:,np.newaxis], np.arange(train_X.shape[1])]
train_Y = train_Y[indices[:,np.newaxis], np.arange(train_Y.shape[1])]

tr_len = int(float(train_X.shape[0] * 80.0 / 100.0))
total_len = train_X.shape[0]

test_X = train_X[tr_len + 1:total_len, :]
test_Y = train_Y[tr_len + 1:total_len, :]

train_X = train_X[1:tr_len, :]
train_Y = train_Y[1:tr_len, :]

train_Y -= 1
test_Y  -= 1

if is_binary:
    train_Y[train_Y <= 14] = 0
    train_Y[train_Y > 14] = 1

    test_Y[test_Y <= 14] = 0
    test_Y[test_Y > 14] = 1

print("test_Y")
print(test_Y)

class_num = len(np.unique(train_Y))
print("class num ")
print(class_num)

train_Y = to_categorical(train_Y)
test_Y  = to_categorical(test_Y)

train_X = np.squeeze(train_X)
test_X = np.squeeze(test_X)

print("shape of trainX and testX")
print(train_X.shape)
print(test_X.shape)

use_dropout = False
model = Sequential()

pool_size = 1
maxpool = MaxPooling1D
act = "relu"
from tensorflow.keras import regularizers
#, kernel_constraint= MinMaxNorm(min_value=-10.0, max_value=10.0, rate=1.0, axis=0)
model.add(Dense(32, input_dim=input_size
                                        , kernel_regularizer=regularizers.l2(1e-7)
                                        , activity_regularizer=regularizers.l1(1e-7)
                ))
model.add(BatchNormalization())
model.add(Activation(act))
model.add(Dropout(0.4))

#model.add(Flatten())
#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))
#model.add(Dense(128, activation=act))
#model.add(BatchNormalization())
#model.add(Activation(act))
#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))
#model.add(Dense(64, activation=act))
#model.add(BatchNormalization())
#model.add(Activation(act))
#model.add(Dropout(0.5))
#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))
#model.add(Dense(64, activation=act))
#model.add(BatchNormalization())
#model.add(Activation(act))
#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))
#model.add(Dense(64,  activation=act))
#model.add(BatchNormalization())
#model.add(Activation(act))
#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))

if is_binary:
    model.add(Dense(class_num, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['binary_accuracy'])
else:
    model.add(Dense(class_num, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['categorical_accuracy'])

print(model.summary())
model.fit(train_X, train_Y, validation_data=(test_X, test_Y), epochs=10000000, batch_size=4, shuffle=True)
model.save_weights("model.h5")
#model.evaluate(test_X, test_Y)
