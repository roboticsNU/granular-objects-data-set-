from tensorflow.keras.utils import to_categorical
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA

class MyData:
    def __init__(self):
        self.test_X = []
        self.test_Y = []
        self.train_X = []
        self.train_Y = []
        self.class_num = 0


# selected_binary0 must be < selected_binary1
def load_data(use_fft=True, fft_size = 64, data_type = "ed_data_x", classes='binary', use_pca=False, pca_n_components=100, selected_binary=False, selected_binary0=0, selected_binary1=1, test_data = False):

    if test_data:
        class_num = 10
        train_X = np.ones((1000, 4000))
        train_Y = np.ones((1000, 1))
        counter = -1
        for row in range(100):
            for label in range(10):
                counter += 1
                for j in range(4000):
                    train_X[counter, j] = label

                train_Y[counter] = label

        # print(train_X)
        # print(train_Y)
    else:
        train_X = pd.read_csv("../full_dataset/" + data_type + ".csv")
        train_Y = pd.read_csv('../full_dataset/labels.csv')
        train_X = train_X.to_numpy()
        train_Y = train_Y.to_numpy()
        if use_fft:
            input_size = fft_size
            train_X = np.fft.fft(train_X, fft_size, 1)
            train_X = np.abs(train_X)
        else:
            input_size=train_X.shape[1]

    train_X = np.reshape(train_X, (train_X.shape[0], train_X.shape[1], 1))

    indices = np.random.permutation(train_X.shape[0])
    train_X = train_X[indices[:, np.newaxis], np.arange(train_X.shape[1])]
    train_Y = train_Y[indices[:, np.newaxis], np.arange(train_Y.shape[1])]

    tr_len = int(float(train_X.shape[0] * 80.0 / 100.0))
    total_len = train_X.shape[0]

    test_X = train_X[tr_len + 1:total_len, :]
    test_Y = train_Y[tr_len + 1:total_len, :]

    train_X = train_X[1:tr_len, :]
    train_Y = train_Y[1:tr_len, :]

    train_Y -= 1
    test_Y -= 1

    if classes == 'binary':
        if selected_binary:
            train_X = [train_X[i, :] for i in train_Y if i in [selected_binary0, selected_binary1]]
            test_X  = [test_X[i, :] for i in test_Y  if i in [selected_binary0, selected_binary1]]

            train_Y = [i for i in train_Y if i in [selected_binary0, selected_binary1]]
            test_Y  = [i for i in test_Y  if i in [selected_binary0, selected_binary1]]

            train_X = np.array(train_X)
            train_Y = np.array(train_Y)
            test_X = np.array(test_X)
            test_Y = np.array(test_Y)

            test_Y[test_Y == selected_binary0] = 0
            test_Y[test_Y == selected_binary1] = 1

            train_Y[train_Y == selected_binary0] = 0
            train_Y[train_Y == selected_binary1] = 1

        else:
            for i in range(train_Y.shape[0]):
                if train_Y[i] in [i for i in range(15)]:
                    train_Y[i] = 0
                else:
                    train_Y[i] = 1

            for i in range(test_Y.shape[0]):
                if test_Y[i] in [i for i in range(15)]:
                    test_Y[i] = 0
                else:
                    test_Y[i] = 1

    elif classes == 'first_15':
        train_X = [train_X[i, :] for i in train_Y if i <= 14]
        test_X  = [test_X[i, :]  for i in test_Y  if i <= 14]
        train_Y = [i for i in train_Y if i <= 14]
        test_Y  = [i for i in test_Y  if i <= 14]

    elif classes == 'last_15':
        train_X = [train_X[i, :] for i in train_Y if i > 14]
        test_X  = [test_X[i, :]  for i in test_Y  if i > 14]
        train_Y = [i for i in train_Y if i > 14]
        test_Y  = [i for i in test_Y  if i > 14]
        test_Y  -= min(test_Y)
        train_Y -= min(train_Y)

    class_num = len(np.unique(train_Y))

    train_Y = to_categorical(train_Y)
    test_Y = to_categorical(test_Y)

    train_X = np.squeeze(train_X)
    test_X = np.squeeze(test_X)

    if use_pca:
        input_size = pca_n_components
        pca = PCA(n_components=pca_n_components)
        train_X = pca.fit_transform(train_X)
        test_X = pca.transform(test_X)
        print(pca.explained_variance_ratio_)


        # normalizing
    # test_X = (test_X - train_X.min(0)) / train_X.ptp(0)
    # train_X = (train_X - train_X.min(0)) / train_X.ptp(0)

    data = MyData()
    data.test_X = test_X
    data.test_Y = test_Y
    data.train_X = train_X
    data.train_Y = train_Y
    data.class_num = class_num
    data.input_size = input_size
    return data