%% read all csv files 
cd /home/togzhan/rosbag/31_07_19
data = gripper();
%%  find min length minVal = 243436
for i = 1:length(data)
   Data = data{i};
   l(1,i) = length(Data);
%    subplot(3,10,i)
%    plot(Data(:,1))
   minVal = min(l);
end
%%
 for i =1:30
Data = data{i};
data{i} = Data(1:243436,:);
hold on
figure
plot(Data(:,1))
 end
%% make all trials of same length 
for i = 1:length(pressure) % #of obj
    a = pressure{1,i};
    b = pressure{2,i};
    c = pressure{2,i};
    for j = 1 : length(a)% #of trial
        A = a{j};
        B = b{j};
        C = c{j};
        DATA{1,i}{j} = A(1, (length(A)-7459):end); 
        DATA{2,i}{j} = B(1, (length(A)-7459):end); 
        DATA{3,i}{j} = C(1, (length(A)-7459):end); 
        if j == 30
            DATA{1,i}{j} = A(1, 1:7460); 
            DATA{2,i}{j} = B(1, 1:7460); 
            DATA{3,i}{j} = C(1, 1:7460); 
        end
    end
end
%% change the format of the data into cells
for i = 1:length(DATA) % #of obj
    a = DATA{1,i};
    b = DATA{2,i};
    c = DATA{2,i};
    for j = 1 : length(a) % #of trial
        A = a{j};
        B = b{j};
        C = c{j};
        for k = 1 : length(A)
            DT{1,i}(j,k) = A(1, k); 
            DT{2,i}(j,k) = B(1, k); 
            DT{3,i}(j,k) = C(1, k); 
        end
    end
end
%% save data as matrix 
m = 1;
for i = 1:length(DT) % #of obj
    a = DT{1,i};
    b = DT{2,i};
    c = DT{3,i};
    for j = 1 : 30 % #of trial
        for l = 1:length(a)
           Pressure(m,l) = a(j,l); 
           AccelX(m,l) = b(j,l);
           AccelY(m,l) = c(j,l);
        end
        m = m + 1;
    end
    
end
%% 
m = 1;
for i = 0:29
    for j = 1:30
        y_train(m,1) = i;
        m=m+1;
    end
end
y_train = [1; y_train];
%% change the length of all trials to 5000
for i = 1:15
    for j = 1:15
        data{1,i}(j,:) = DT{1,i}(j,2001:7000); 
        data{2,i}(j,:) = DT{2,i}(j,2001:7000);
        data{3,i}(j,:) = DT{3,i}(j,2001:7000);
        
    end
end
for i = 1:15
subplot(3,5,i)
plot(data{1,1}(i,:))
end
%%  ##### edit gripper data ##### %% change length of the data to 4000 
m=1;
for i = 1:30
    for j = 1:30
        if i == 1||i == 2||i == 3||i == 4||i == 5||i == 6||i == 7||i == 8||i == 9||i == 10
            if j ==30
                ed_data_x(m,:) = data(m,3001:7000);
                ed_data_y(m,:) = data_y(m,3001:7000);
                ed_pressure(m,:) = Pressure(m,3001:7000);
                m=m+1;
            else
                ed_data_x(m,:) = data(m,2201:6200);
                ed_data_y(m,:) = data_y(m,2201:6200);
                ed_pressure(m,:) = Pressure(m,2201:6200);
                m=m+1;
            end
        elseif  i == 29||i == 30
            if j ==30
                ed_data_x(m,:) = data(m,3461:7460);
                ed_data_y(m,:) = data_y(m,3461:7460);
                ed_pressure(m,:) = Pressure(m,3461:7460);
                m=m+1;
            else
                ed_data_x(m,:) = data(m,3201:7200);
                ed_data_y(m,:) = data_y(m,3201:7200);
                ed_pressure(m,:) = Pressure(m,3201:7200);
                m=m+1;
            end
            
        else
             if j ==30
                ed_data_x(m,:) = data(m,3401:7400);
                ed_data_y(m,:) = data_y(m,3401:7400);
                ed_pressure(m,:) = Pressure(m,3401:7400);
                m=m+1;
            else
                ed_data_x(m,:) = data(m,2801:6800);
                ed_data_y(m,:) = data_y(m,2801:6800);
                ed_pressure(m,:) = Pressure(m,2801:6800);
                m=m+1;
             end
        end
    end
end
%% testing training
m=1;m2=1;
for i = 0:29
    training_20_x{m,1} = ed_data_x((1+i*30):((i+1)*30-10),:);
    testing_10_x{m2,1} = ed_data_x(((i+1)*30-9):((i+1)*30),:);
    training_20_y{m,1} = ed_data_y((1+i*30):((i+1)*30-10),:);
    testing_10_y{m2,1} = ed_data_y(((i+1)*30-9):((i+1)*30),:);
    training_20_p{m,1} = ed_pressure((1+i*30):((i+1)*30-10),:);
    testing_10_p{m2,1} = ed_pressure(((i+1)*30-9):((i+1)*30),:);
    m=m+1;
    m2=m2+1;
end
%% training data matrix
m=1;
for i = 1:30
    for j = 1:20
       training_data_x (m,:) = training_20_x{i,1}(j,:);
       training_data_y (m,:) = training_20_y{i,1}(j,:);
       training_data_p (m,:) = training_20_p{i,1}(j,:);
       m=m+1;
    end
end 
%% testing  data matrix
m=1;
for i = 1:30
    for j = 1:10
       testing_data_x (m,:) = testing_10_x{i,1}(j,:);
       testing_data_y (m,:) = testing_10_y{i,1}(j,:);
       testing_data_p (m,:) = testing_10_p{i,1}(j,:);
       m=m+1;
    end
end 
%% normalize training testing data 
training_data_x=normal(training_data_x);
training_data_y=normal(training_data_y);
training_data_p=normal(training_data_p);
testing_data_x=normal(testing_data_x);
testing_data_y=normal(testing_data_y);
testing_data_p=normal(testing_data_p);
%% add headers for each column 
for i = 1:4000
    header(1,i) = i;
end 
training_data_x=[header; training_data_x];
training_data_y=[header;training_data_y];
training_data_p=[header;training_data_p];
testing_data_x=[header;testing_data_x];
testing_data_y=[header;testing_data_y];
testing_data_p=[header;testing_data_p];
%% save data csv format 
csvwrite('training_data_x.csv',training_data_x);
csvwrite('training_data_y.csv',training_data_y);
csvwrite('training_data_p.csv',training_data_p);
csvwrite('testing_data_x.csv',testing_data_x);
csvwrite('testing_data_y.csv',testing_data_y);
csvwrite('testing_data_p.csv',testing_data_p);
%% FUNCTION
function Data = gripper(~)
fileNames = dir('*.csv');
filenames = {fileNames.name}; % loads all csv files 
for i = 1:length(filenames)
    data = csvread(filenames{i},1,7);
    k = 1;
    state = data(:,4);
    for j =1 : length(data)
        if data(j,4) == 0
            range(k) = j;
            k = k+1;
        end
    end
    Data{1,i} = data(k:end,:);
end
end