%% #### take 2 obj soda-5 and nut-1 ##### %%
cd /home/togzhan/rosbag/data
load('Data.mat'); %% length 5000
sn_p  = [Data{1,1};Data{1,5}];
sn_ax = [Data{2,1};Data{2,5}];
sn_ay = [Data{3,1};Data{3,5}];
%%
for i = 1:20
    s(i,1) = 0;
    n(i,1) = 1;
end
Sn_y = [1;s;n];
%%
sn_ax =[sn_y sn_ax];
sn_ay =[sn_y sn_ay];
sn_p =[sn_y sn_p];
%% add header
for i = 1:length(sn_p)
header(1,i) = i; 
end
sn_p = [header; sn_p];
sn_ax = [header; sn_ax];
sn_ay = [header; sn_ay];
%% 60% train 20% validation 20%test
Sn_p  = [sn_p(1:21,:);sn_p(32:51,:)];
Sn_ax = [sn_ax(1:21,:);sn_ax(32:51,:)];
Sn_ay = [sn_ay(1:21,:);sn_ay(32:51,:)];%% train
SN_p  = [header;sn_p(22:31,:);sn_p(52:end,:)];
SN_ax = [header;sn_ax(22:31,:);sn_ax(52:end,:)];%% test
SN_ay = [header;sn_ay(22:31,:);sn_ay(52:end,:)];
%% compute fft oof sn_norm
fft_sn_norm = compute_fft(sn_n);
%% compute fft of 15 norm obj
norm_15 = normal(obj_15);
fft_15 = compute_fft(norm_15);
train_fft_15 = [y fft_15];
% compute fft of 15 obj raw data
fft_15_raw = compute_fft(obj_15);
train_fft_15_raw = [y fft_15_raw];
% compute fft 2obj raw data
fft_sn_raw = compute_fft(sn);
train_fft_sn_raw = [sny fft_sn_raw];
% compute fft all obj raw data
fft_all = compute_fft(train_all);

%% label for without ball/with ball
for i = 1:300
y_tr(i,1) = 0;
y2(i,1 )= 1;
end 
for i = 1:150
y_test(i,1) = 0;
y3(i,1 )= 1;
end 
%%
y_tr= [y_tr; y2];
y_test= [y_test; y3];
%% testing training data for classifier 
trainX = [ytraining training_data_x];
testX = [ytesting testing_data_x];
%% compute fft 
fft_trainX= compute_fft(training_data_x);
fft_testX = compute_fft(testing_data_x);
fft_trainY= compute_fft(training_data_y);
fft_testY = compute_fft(testing_data_y);

%% header for fft 
for i = 1:2049
    headerFFT(1,i) = i;
end 
%% save data 
csvwrite('fft_trainX.csv',[headerFFT; fft_trainX]);
csvwrite('fft_testX.csv',[headerFFT; fft_testX ]);
csvwrite('fft_trainY.csv',[headerFFT; fft_trainY ]);
csvwrite('fft_testY.csv',[headerFFT; fft_testY]);

%% #### load data #####  %%
data2 = csvread('training_data.csv');
ytrain = csvread('y_train.csv');
ytrain=ytrain(2:end,:);
%%
ytesting = csvread('y_testing.csv');
ytraining = csvread('y_training.csv');
testing_data_x = csvread('testing_data_x.csv');
testing_data_y = csvread('testing_data_y.csv');
testing_data_p = csvread('testing_data_p.csv');
training_data_x = csvread('training_data_x.csv');
training_data_y = csvread('training_data_y.csv');
training_data_p = csvread('training_data_p.csv');
fft_trainX = csvread('fft_trainX.csv');
fft_testX = csvread('fft_testX.csv');
fft_trainY = csvread('fft_trainY.csv');
fft_testY = csvread('fft_testY.csv');

% remove header 
ytraining=ytraining(2:end,:);
ytesting=ytesting(2:end,:);

training_data_x=training_data_x(2:end,:);
testing_data_x=testing_data_x(2:end,:);
training_data_y=training_data_y(2:end,:);
testing_data_y=testing_data_y(2:end,:);
training_data_p = training_data_p(2:end,:);
testing_data_p = testing_data_p(2:end,:);
fft_trainX = fft_trainX(2:end,:);
fft_testX = fft_testX(2:end,:);
fft_trainY = fft_trainY(2:end,:);
fft_testY = fft_testY(2:end,:);
%%
fft_trainX = [y_tr fft_trainX];
fft_testX = [fft_testX];
fft_trainY = [y_tr fft_trainY];
fft_testY = [fft_testY];
%% spectogram 
noverlap = 50; % overlap samples 
L = 64;
w = hamming(L);
for i = 93
    [s,f,t,p] = spectrogram(training(i,:), w,noverlap,4000,4000);
end
%% #### FUNCTIONS #### %%
%% fft compute f-n for matrix input 
function dif = compute_fft(data) % A - lowerbound B- upperbound C-csv fie name
l = size(data);
for i = 1:l(1,1)
    y=data(i,:)-mean(data(i,:));y=detrend(y);
    Fs = 4000;
    Fnorm = 5/(Fs/2); % Normalized frequency
    df = designfilt('highpassiir',...
               'PassbandFrequency',Fnorm,...
               'FilterOrder',2,...
               'PassbandRipple',1,...
               'StopbandAttenuation',100);
    z = filter(df,y);
    L=length(z);
    NFFT = 2^nextpow2(L); % Next power of 2 from length of y
    Y = fft(z,NFFT)/L;
    f = Fs/2*linspace(0,1,NFFT/2+1);
    P=2*abs(Y(1:NFFT/2+1));
    dif(i,:) = P.';
end
end
%% for matrix input 
function normalized = normal(data)
l = size(data);
for i = 1:l(1,1)
    maxim(i) = max(data(i,:)); 
    minim(i) = min(data(i,:));
end
Max = max(maxim);
Min = min(minim);
dif = Max - Min;
for i = 1:l(1,1)
    a = data(i,:);
    for j = 1:length(a)
        if a(1,j) == Min
            normalized(i,j) = 0;
        elseif a(1,j) == Max
            normalized(i,j) = 1;
        else
            normalized(i,j) = (a(1,j) - Min)./dif;
        end
    end
end 
end
%%
function normalized = normal(data)
l = size(data);
for i = 1:l(1,1)
    maxim(i) = max(data(i,:)); 
    minim(i) = min(data(i,:));
end
Max = max(maxim);
Min = min(minim);
dif = Max - Min;
for i = 1:l(1,1)
    a = data(i,:);
    for j = 1:length(a)
        if a(1,j) == Min
            normalized(i,j) = 0;
        elseif a(1,j) == Max
            normalized(i,j) = 1;
        else
            normalized(i,j) = (a(1,j) - Min)./dif;
        end
    end
end 
end

function dif = compute_fft_window(data,window) % A - lowerbound B- upperbound C-csv fie name
l = size(data);
for i = 1:l(1,1) %each row 
    for j = 1:floor(length(data)/window)
        d = data(i,1+window*(j-1):window*j);
        y=d-mean(d);y=detrend(y);
        Fs = 4000;
        Fnorm = 5/(Fs/2); % Normalized frequency
        df = designfilt('highpassiir',...
               'PassbandFrequency',Fnorm,...
               'FilterOrder',2,...
               'PassbandRipple',1,...
               'StopbandAttenuation',100);
        z = filter(df,y);
        L=length(z);
        NFFT = 2^nextpow2(L); % Next power of 2 from length of y
        Y = fft(z,NFFT)/L;
        f = Fs/2*linspace(0,1,NFFT/2+1);
        P=2*abs(Y(1:NFFT/2+1));
        P_2=P.^2;
        P_3=(P_2).*f;
        SC = sum(P_3)./(sum(P_2));
        PP =(1/L)*(sum(z.^2)); 
        dif(i,j) = SC;
    end
end
end