#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.keras.layers import LSTM ,Dense,Embedding,Flatten,Dropout,TimeDistributed,Activation,MaxPooling1D,MaxPooling2D,BatchNormalization
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.constraints import MinMaxNorm
from tensorflow.keras.initializers import RandomNormal
from data_loader import load_data

import logging

learning_rate = 1e-4
opt = tf.keras.optimizers.SGD(lr=learning_rate, momentum=0.85, decay=0.001)
#opt = tf.keras.optimizers.SGD(lr=learning_rate, momentum=0.1)
#opt = tf.keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
logging.getLogger('tensorflow').disabled = True
classes = 'binary'
data = load_data(fft_size = 4000, data_type = 'ed_data_x', classes=classes)
train_X = data.train_X
test_X = data.test_X

train_Y = data.train_Y
test_Y = data.test_Y

class_num = data.class_num
input_size = data.input_size

print("class num: " + str(class_num))
print("input_size: " + str(input_size))
print("train_Y shape: " + str(train_Y.shape))
print("train_X shape: " + str(train_X.shape))
print("test_Y shape: " + str(test_Y.shape))
print("test_X shape: " + str(test_X.shape))

# model building
model = Sequential()
act = "tanh"
from tensorflow.keras import regularizers
#, kernel_constraint= MinMaxNorm(min_value=-10.0, max_value=10.0, rate=1.0, axis=0)
model.add(Dense(32, input_dim=input_size
                                        , kernel_regularizer=regularizers.l2(1e-5)
                                        , activity_regularizer=regularizers.l1(1e-5)
                ))
model.add(BatchNormalization())
model.add(Activation(act))
model.add(Dropout(0.4))
#model.add(Flatten())
#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))
#model.add(Dense(128, activation=act))
#model.add(BatchNormalization())
#model.add(Activation(act))

if classes == "binary":
    model.add(Dense(class_num, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['binary_accuracy'])
else:
    model.add(Dense(class_num, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['categorical_accuracy'])

print(model.summary())
model.fit(train_X, train_Y, validation_data=(test_X, test_Y), epochs=10000000, batch_size=4, shuffle=True)
model.save_weights("model.h5")
#model.evaluate(test_X, test_Y)
