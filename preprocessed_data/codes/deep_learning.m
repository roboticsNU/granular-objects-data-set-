numHiddenUnits = 100;
numClasses=2;
numFeatures = 1;
layers = [ ...
    sequenceInputLayer(numFeatures)
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    dropoutLayer(0.2)
    lstmLayer(numHiddenUnits,'OutputMode','last')
    dropoutLayer(0.2)
    fullyConnectedLayer(numClasses)
    softmaxLayer
    classificationLayer];
maxEpochs = 300;
miniBatchSize =128 ;%128 default
options = trainingOptions('rmsprop', ...
    'ExecutionEnvironment','cpu', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'LearnRateDropFactor',0.1,...
    'LearnRateDropPeriod',1,...
    'GradientThreshold',1, ...
    'Verbose',0, ...
    'Plots','training-progress');
net = trainNetwork(X_train, Y_train,layers,options); %%x train - cell y train - categorical
%%
sn_norm = csvread('sn_norm.csv');
l = size(sn_norm);
for i = 1:l(1,1)-1
    X_train{i,1} = sn_norm(i+1,:);
end
%%
Y_train = categorical(sny);