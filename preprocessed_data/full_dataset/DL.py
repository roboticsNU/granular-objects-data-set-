#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.keras.layers import LSTM ,Dense,Embedding,Flatten,Dropout,TimeDistributed,Activation,MaxPooling1D,MaxPooling2D,BatchNormalization,Conv1D,Embedding
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.constraints import MinMaxNorm
from tensorflow.keras.initializers import RandomNormal
from loader import load_data

import logging

# below is 85-88-90 vs >90 loss 0.3  binary case old data
#learning_rate = 1e-1 #
#opt = tf.keras.optimizers.SGD(lr=learning_rate, momentum=0.1, decay=0.01) #
# end

learning_rate = 1e-5 #
opt = tf.keras.optimizers.SGD(lr=learning_rate, momentum=0.3, decay=0.01) #
#opt = tf.keras.optimizers.Adam(lr=learning_rate)

logging.getLogger('tensorflow').disabled = True
classes = 'binary'
# data = load_data(fft_size = 8000, data_type = 'ed_pressure', classes=classes) # good for binary
pca_n_components = 100
data = load_data(use_fft=False, fft_size = 4000, data_type = 'data_X', classes=classes, use_pca=False, pca_n_components=pca_n_components, selected_binary=False, selected_binary0=1, selected_binary1=4)
train_X = data.train_X
test_X  = data.test_X

train_Y = data.train_Y
test_Y  = data.test_Y

class_num = data.class_num
input_size = data.input_size

print("class num: " + str(class_num))
print("input_size: " + str(input_size))
print("train_Y shape: " + str(train_Y.shape))
print("train_X shape: " + str(train_X.shape))
print("test_Y shape: " + str(test_Y.shape))
print("test_X shape: " + str(test_X.shape))

# model building
model = Sequential()
act = "tanh" # for binary old data tanh
from tensorflow.keras import regularizers
#, kernel_constraint= MinMaxNorm(min_value=-10.0, max_value=10.0, rate=1.0, axis=0)
conv = False
lstm = True

if lstm:
    features_per_time_step = 100
    train_X = train_X.reshape((train_X.shape[0], int(train_X.shape[1] / features_per_time_step), features_per_time_step))
    test_X  = test_X.reshape((test_X.shape[0], int(test_X.shape[1] / features_per_time_step), features_per_time_step))

    model.add(LSTM(20, return_sequences=True, activation='relu', input_shape=(train_X.shape[1], train_X.shape[2])))
    model.add(BatchNormalization())
    model.add(Flatten())
elif conv:
    train_X = np.expand_dims(train_X, axis=2)
    test_X  = np.expand_dims(test_X,  axis=2)

    model.add(Conv1D(2, (3), strides=1, activation='relu', input_shape=(train_X.shape[1], train_X.shape[2])))
    model.add(Flatten())
else:
    model.add(Dense(8, input_dim=input_size # size 64 for binary old data
                                            , kernel_regularizer  =regularizers.l2(1e-10)      # 1e-10 for binary old data
                                            , activity_regularizer=regularizers.l1(1e-10)    # 1e-10 for binary old data
                    ))
    model.add(BatchNormalization())
    model.add(Activation(act))
    model.add(Dropout(0.1))

#model.add(maxpool(pool_size=pool_size, strides=None, padding='valid', data_format='channels_last'))
model.add(Dense(32

                , kernel_regularizer=regularizers.l2(1e-10)  # 1e-10 for binary old data
                , activity_regularizer=regularizers.l1(1e-10)  # 1e-10 for binary old data
                ))
model.add(BatchNormalization())
model.add(Activation(act))
model.add(Dropout(0.1))

if classes == "binary":
    model.add(Dense(class_num, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['binary_accuracy'])
else:
    model.add(Dense(class_num, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['categorical_accuracy'])

print(model.summary())
model.fit(train_X, train_Y, validation_data=(test_X, test_Y), epochs=10000000, batch_size=4, shuffle=True)
model.save_weights("model.h5")
#model.evaluate(test_X, test_Y)
