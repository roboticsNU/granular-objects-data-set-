Object labels:

1 - rice 
2 - soda 
3 - salt 
4 - nut
5 - beans

Sensor details: 

1. SENSOR POSITION - sensor was placed on the gripper. 
2. DAQ was reading data with frequency of 1000Hz. Barometer sampling rate - 100Hz. Accelerometer and barometer data was recorded with sampling rate of 1000Hz.
 
data_trial_grip_on2 - contains  5 cells(5 objects) :
1. each of 5 cells contains 29 cells (29 trials) . 
2. in each of 29 cells: 1st column - pressure data
3. 2nd column - accelerometer data 

Data_on2 - cell:
1. 5 cells correspond to the objects according to the labels. 
2. each of 5 cells contains 29 cell( means 29 trials). 
3. each of 29 cells contains fft: 1st row: corresponds to the frequency. 2nd row to the magnitude.

