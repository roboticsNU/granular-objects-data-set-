Object labels:
1 - kuskus
2 - salt
3 - beans
4 - soda
5 = buckwheat
6 - nut
7 - peas
8 - lentils
9 - rice

Sensor details: 

1. SENSOR POSITION - sensor was placed on the gripper. 
2. DAQ was reading data with frequency of 8000Hz. Barometer sampling rate - 100Hz. Accelerometer and barometer data was recorded with sampling rate of 5000Hz.
 
data_trial_grip_on - contains  9 cells(9 objects) :
1. each of 5 cells contains 29 cells (29 trials) . 
2. in each of 29 cells: 1st column - pressure data
3. 2nd column - accelerometer data 

Data_on - cell:
1. 9 cells correspond to the objects according to the labels. 
2. each of 9 cells contains 29 cell( means 29 trials). 
3. each of 29 cells contains fft: 1st row: corresponds to the frequency. 2nd row to the magnitude.
