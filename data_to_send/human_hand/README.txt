1. Human was squeezing 9 objects (beans, buckwheat,kuskus, lentils,nut,peas,rice,salt,soda)
at 5 different velocities corresponding to 60,80,100,120,140 bpm (beats per minute)
2. Data saved in *.mat format and named corresponding to the objects.
3. Each *.mat file has following structure: 
   3.1 example: buckwheat_data - type cell, contains 5 cells, each corresponding to different velocities in following sequence 100, 120, 140, 60, 80.
   3.2 each of 5 cells contains from 18 to 30 cells(contains matrix of 2 columns, where 1st column - data from accelerometer; 2nd column - barometer data)
   that represents the number of trials. 

Sensor details: 

1. DAQ was reading data with frequency of 8000Hz. Barometer sampling rate - 100Hz. Accelerometer and barometer data was recorded with sampling rate of 5000Hz.
2. sensors were attached on the fingertip of the glove. 
   